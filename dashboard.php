<?php require 'inc/init.php';

$sensors = $U->getSensors();
$sensors_count = !$sensors ? "0" : count($sensors) ;
$dashboard_sensors = $sensors_count == "0" ? false : $U->getDashboardSensors($sensors) ;
$dashboard_sensors_count = !$dashboard_sensors ? false : count($dashboard_sensors);

require 'header.php'; ?>

<div class="content-wrapper">

	<div class="container py-2">

		<div class="content-header">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="m-0"><?= tr_('Dashboard'); ?> (<?= $dashboard_sensors_count ? : "0" .'/'. $sensors_count ? : "0" ?>)</h1>
				</div>
			</div>
		</div>

		<div id="alerts"></div>

		<div class="content">
			<div class="row">
				<?php if ($dashboard_sensors){$dashboard_sensors = array();foreach($dashboard_sensors as $sensor){
					$S = new Sensor($sensor['id']); ?>
					<div class="col-12 col-md-4 ">
			            <div class="info-box">
			           		<span class="info-box-icon">
			           			<img src="<?= getProductImage($sensor['sensor_product_id']);?>" height="150px">
			           		</span>
		            		<div class="info-box-content">
		            			<code>#<?= $sensor['numeroSerie']; ?></code>
		            			<span class="info-box-text"><strong><?= Str::truncate($sensor['nom']); ?></strong></span>
		                		<span class="progress-description">
		                			<?php $managers_id = $S->getManagers();
		                			if( $managers_id ) {
										foreach( explode(",", $managers_id ) as $manager_id){
											$manager = $U->get($manager_id);
											echo '<div style="display:flex; align-items: center;">';
											echo '<div>';
											echo '<img src="images/avatar.png" alt="'.$manager['prenom'].' '.$manager['nom'].'" class="img-circle img-size-32 mr-2">';
											echo '</div>';
											echo '<div style="flex-flow: column;line-height: normal;">';
											echo '<p class="mb-0">'.$manager['prenom'].' '.$manager['nom'].'</p>';
											echo '<small>'.$manager['societe'].'</small>';
											echo '</div>';
											echo '</div>';
										}
									} ?>
									<?php $users_id = $S->getUsers();
									if( $users_id ) {
										foreach( explode(",", $users_id ) as $user_id){
											$user = $U->get($user_id);
											echo '<div style="display:flex; align-items: center;">';
											echo '<div>';
											echo '<img src="images/avatar.png" alt="'.$user['prenom'].' '.$user['nom'].'" class="img-circle img-size-32 mr-2">';
											echo '</div>';
											echo '<div style="flex-flow: column;line-height: normal;">';
											echo '<p class="mb-0">'.$user['prenom'].' '.$user['nom'].'</p>';
											echo '<small>'.$user['societe'].'</small>';
											echo '</div>';
											echo '</div>';
										}
									} ?>
		                		</span>
		                		<a href="sensor.php?id=<?= $sensor['id'] ;?>" class="btn btn-default btn-sm mt-2" ><?= tr_('View'); ?></a>
		              		</div>
			            </div>
			        </div>
				<?php }} ?>
			</div>
		</div>

	</div>

</div>

<?php require 'footer.php'; ?>