<?php
class Sensor{
    private $db;
	private $sensor_id;

	public function __construct(int $sensor_id){
		$this->db = App::getDatabase();
		$this->sensor_id = $sensor_id;
	}

	public function get(int $sensor_id = NULL){
		$sensor_id = $sensor_id ? : $this->sensor_id;
	    $r = $this->db->query("SELECT * FROM objet WHERE id = $sensor_id")->fetch();
	    return $r ? : NULL;
	}

	function getLatestData(string $num){
	    $r = $this->db->query("SELECT * FROM Sinafis WHERE Sdevice = '$num' ORDER BY Sid DESC LIMIT 1" )->fetch();
	    return $r ? : NULL;
	}

	function getManagers(int $sensor_id = NULL){
		$sensor_id = $sensor_id ? : $this->sensor_id;
	    $r = $this->db->query("SELECT sensor_manager_ids FROM objet WHERE id = $sensor_id")->fetch();
	    return $r['sensor_manager_ids'] ? : NULL;
	}

	function getUsers(int $sensor_id = NULL){
		$sensor_id = $sensor_id ? : $this->sensor_id;
	    $r = $this->db->query("SELECT sensor_user_ids FROM objet WHERE id = $sensor_id")->fetch();
	    return $r['sensor_user_ids'] ? : NULL;
	}

	function setManager(int $user_id, int $sensor_id = NULL){
		$sensor_id = $sensor_id ? : $this->sensor_id;
	    $user_ids = $this->getManagers($sensor_id);
	    if($user_ids){
	        $users = explode(",", $user_ids);
	        if ( !in_array($user_id, $users) ) {
	            array_push($users, $user_id);
	        }
	        $value = implode(",", $users);
	    }else{
	        $value = $user_id;
	    }
	    return $this->db->query("UPDATE objet SET sensor_manager_ids = '$value' WHERE id = $sensor_id");
	}

	function setUser(int $user_id, int $sensor_id = NULL){
		$sensor_id = $sensor_id ? : $this->sensor_id;
	    $user_ids = $this->getUsers($sensor_id);
	    if($user_ids){
	        $users = explode(",", $user_ids);
	        if ( !in_array($user_id, $users) ) {
	            array_push($users, $user_id);
	        }
	        $value = implode(",", $users);
	    }else{
	        $value = $user_id;
	    }
	    return $this->db->query("UPDATE objet SET sensor_user_ids = '$value' WHERE id = $sensor_id");
	}

	function setToDashboard(bool $state, int $sensor_id = NULL){
		$sensor_id = $sensor_id ? : $this->sensor_id;
	    return $this->db->query("UPDATE objet SET affichable = '$state' WHERE id = $sensor_id");
	}

	function removeManager(int $user_id, int $sensor_id = NULL){
		$sensor_id = $sensor_id ? : $this->sensor_id;
        $user_ids = getManagers($sensor_id);
        if($user_ids){
            $users = explode(",", $user_ids);
            if ( ($key = array_search($user_id, $users) ) !== false) {
                unset($users[$key]);
            }
        }
        $value = implode(",", $users);
        return $this->db->query("UPDATE objet SET sensor_manager_ids = '$value' WHERE id = $sensor_id");
    }

	function removeUser(int $user_id, int $sensor_id = NULL){
		$sensor_id = $sensor_id ? : $this->sensor_id;
	    $user_ids = $this->getUsers($sensor_id);
	    if($user_ids){
	        $users = explode(",", $user_ids);
	        if ( ($key = array_search($user_id, $users) ) !== false) {
	            unset($users[$key]);
	        }
	    }
	    $value = count($users) > 0 ? implode(",", $users) : NULL;
	    return $this->db->query("UPDATE objet SET sensor_user_ids = '$value' WHERE id = $sensor_id");
	}

	public function delete(int $sensor_id = NULL){
		$sensor_id = $sensor_id ? : $this->sensor_id;
	    return $this->db->query("DELETE FROM objet WHERE id = $sensor_id LIMIT 1");
	}


	public function update_settings(array $data, int $sensor_id = NULL){
	    $sensor_id = $sensor_id ? : $this->sensor_id;
	    $nom = $data['nom'] ? : NULL;
	    $desc = $data['description'] ? : NULL;
	    $checktempairmini = isset($data['checktempairmini']) ? 1 : 0;
	    $tempairmini = $data['tempairmini'] ? : NULL;
	    $checktempairmaxi = isset($data['checktempairmaxi']) ? 1 : 0;
	    $tempairmaxi = $data['tempairmaxi'] ? : NULL;
	    $checkhumairmini = isset($data['checkhumairmini']) ? 1 : 0;
	    $humairmini = $data['humairmini'] ? : NULL;
	    $checkhumairmaxi = isset($data['checkhumairmaxi']) ? 1 : 0;
	    $humairmaxi = $data['humairmaxi'] ? : NULL;
	    $checktempsolmini = isset($data['checktempsolmini']) ? 1 : 0;
	    $tempsolmini = $data['tempsolmini'] ? : NULL;
	    $checktempsolmaxi = isset($data['checktempsolmaxi']) ? 1 : 0;
	    $tempsolmaxi = $data['tempsolmaxi'] ? : NULL;
	    $checkhumsolmini = isset($data['checkhumsolmini']) ? 1 : 0;
	    $humsolmini = $data['humsolmini'] ? : NULL;
	    $checkhumsolmaxi = isset($data['checkhumsolmaxi']) ? 1 : 0;
	    $humsolmaxi = $data['humsolmaxi'] ? : NULL;
	    $checktempsol2mini = isset($data['checktempsol2mini']) ? 1 : 0;
	    $tempsol2mini = $data['tempsol2mini'] ? : NULL;
	    $checktempsol2maxi = isset($data['checktempsol2maxi']) ? 1 : 0;
	    $tempsol2maxi = $data['tempsol2maxi'] ? : NULL;
	    $checkhumsol2mini = isset($data['checkhumsol2mini']) ? 1 : 0;
	    $humsol2mini = $data['humsol2mini'] ? : NULL;
	    $checkhumsol2maxi = isset($data['checkhumsol2maxi']) ? 1 : 0;
	    $humsol2maxi = $data['humsol2maxi'] ? : NULL;
	    $checkhumfeuille1mini = isset($data['checkhumfeuille1mini']) ? 1 : 0;
	    $humfeuille1mini = $data['humfeuille1mini'] ? : NULL;
	    $checkhumfeuille1maxi = isset($data['checkhumfeuille1maxi']) ? 1 : 0;
	    $humfeuille1maxi = $data['humfeuille1maxi'] ? : NULL;
	    $affichable = isset($data['checkdashboard']) ? 1 : 0;
	    $adressemail = $data['adressemail'] ? : NULL;
	    return $this->db->query( " UPDATE objet
	                                        SET nom = '$nom',
	                                        description = '$desc',
	                                        seuilMini1Actif = '$checktempairmini',
	                                        seuilMini1 = '$tempairmini',
	                                        seuilMaxi1Actif = '$checktempairmaxi',
	                                        seuilMaxi1 = '$tempairmaxi',
	                                        seuilMini2Actif = '$checkhumairmini',
	                                        seuilMini2 = '$humairmini',
	                                        seuilMaxi2Actif = '$checkhumairmaxi',
	                                        seuilMaxi2 = '$humairmaxi',
	                                        seuilMini3Actif = '$checktempsolmini',
	                                        seuilMini3 = '$tempsolmini',
	                                        seuilMaxi3Actif = '$checktempsolmaxi',
	                                        seuilMaxi3 = '$tempsolmaxi',
	                                        seuilMini4Actif = '$checkhumsolmini',
	                                        seuilMini4 = '$humsolmini',
	                                        seuilMaxi4Actif = '$checkhumsolmaxi',
	                                        seuilMaxi4 = '$humsolmaxi',
	                                        seuilMini5Actif = '$checktempsol2mini',
	                                        seuilMini5 = '$tempsol2mini',
	                                        seuilMaxi5Actif = '$checktempsol2maxi',
	                                        seuilMaxi5 = '$tempsol2maxi',
	                                        seuilMini6Actif = '$checkhumsol2mini',
	                                        seuilMini6 = '$humsol2mini',
	                                        seuilMaxi6Actif = '$checkhumsol2maxi',
	                                        seuilMaxi6 = '$humsol2maxi',
	                                        seuilMini8Actif = '$checkhumfeuille1mini',
	                                        seuilMini8 = '$humfeuille1mini',
	                                        seuilMaxi8Actif = '$checkhumfeuille1maxi',
	                                        seuilMaxi8 = '$humfeuille1maxi',
	                                        affichable = '$affichable',
	                                        email1 = '$adressemail',
	                                        objetEmail1 = 'Alerte de dépassement de seuil'
	                                        WHERE id = $sensor_id");
	}

	public function update_admin_settings(array $data, int $sensor_id = NULL){
	    $sensor_id = $sensor_id ? : $this->sensor_id;
	    $sensor_product_id = isset( $data['sensor_product_id']) ? $data['sensor_product_id'] : 1;
	    $sensor_manager_ids = isset( $data['sensor_manager_ids'] ) ? implode(",", $data['sensor_manager_ids']) : 'NULL' ;
	    $sensor_user_ids = isset( $data['sensor_user_ids'] ) ? implode(",", $data['sensor_user_ids']) : 'NULL';
	    return $this->db->query("UPDATE objet SET sensor_manager_ids = $sensor_manager_ids, sensor_user_ids = $sensor_user_ids, sensor_product_id = $sensor_product_id
	        WHERE id = $sensor_id");
	}


}